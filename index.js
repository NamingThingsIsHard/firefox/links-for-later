/*
 * An addon to save links to look at later.
 * This should help get rid of useless tabs we don't open or click on
 */
import {updateBadge} from "./lib/common.js";
import {RandomGetterTab} from "./background/RandomGetterTab.js"
import {PrivateGetterTab} from "./background/PrivateGetterTab.js"

const storage = browser.storage.local;

/**
 * Information like the title and favicon
 *
 * Create a new inactive tab with the URL in order to compile information about it.
 * Forced to do this because "page-worker.Page" doesn't exist anymore.
 * Love it...
 *
 * @param url
 * @returns {Promise}
 */
function getInformation(url) {
    return new Promise((accept, reject) => {
        storage.get("options").then(({options = {}}) => {
            let timeoutId = null;
            let delay = Number(options.delay);
            let lastMessage = null;
            const getterTab = options.privateWindow ?
                new PrivateGetterTab() : new RandomGetterTab();

            function end(reason) {
                console.info(`End getting information about ${url}`, reason, lastMessage)
                clearTimeout(timeoutId);
                getterTab.stop();
                (lastMessage ? accept : reject)(lastMessage)
            }

            getterTab.listeners.removed.add(end);
            getterTab.listeners.onInformation.add(function onInformation(message) {
                lastMessage = message;
                for (let key in message) {
                    if (!message[key]) {
                        return
                    }
                }
                end()
            })

            if (delay) {
                timeoutId = setTimeout(end, delay * 1000);
            }
            getterTab.start(url).catch(end)
        });

    });
}

/**
 * Params are from https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/menus/OnClickData
 */
function saveLink({srcUrl, linkUrl, pageUrl}) {
    var link = srcUrl || linkUrl || pageUrl;

    /**
     *
     * @param information {Object}
     * @param information.favicon {String=}
     * @param information.title {String=}
     * @returns {Promise<TResult>|*}
     */
    function doSave(information) {
        information = information || {
            favicon: "",
            title: link,
        };
        var linkStore = {};
        linkStore[link] = information;
        return storage.set(linkStore).then(() => {
            updateBadge();
        });
    }

    return getInformation(link).then(doSave, doSave);
}


/**
 * Depending on the type of item clicked, we should change the title
 *
 * TODO move this to the content script
 * @param context {Object}
 */
function predicate(context) {
    console.log("predicate context:", context);
    menuItem.data = context.linkURL;
    if (context.linkURL) {
        menuItem.label = "Save link for later";
    } else {
        menuItem.label = "Save page for later";
    }
    return true
}

var menuItemId = browser.contextMenus.create({
        id: "save-link",
        title: "Save link",
        contexts: [
            "link",
            "page",
            "tab"
        ],
        onclick: saveLink,
    },
    console.log
);

// Temporary migration step
// TODO remove this before FF 57 is released
storage.get().then((results) => {
    if (Object.keys(results).length === 0) {
        browser.runtime.sendMessage("import-legacy-data")
            .then((reply) => {
                if (reply) {
                    for (let key in reply) {
                        saveLink({srcUrl: key})
                    }
                }
                updateBadge();
            })
    } else {
        // Migrate from 1.0.0
        for (let url in results) {
            if (typeof results[url] !== "object") {
                saveLink({srcUrl: url})
            }
        }
        updateBadge();
    }
})

// Cleanup for #21
// TODO remove in next task
browser.contextualIdentities.query({
    name: "tmp_links_for_later"
}).then((identities) => {
    identities.forEach(identity => browser.contextualIdentities.remove(identity.cookiestoreId))
});
