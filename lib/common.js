async function getLinks() {
    const links = await browser.storage.local.get();
    return Object.keys(links)
        .filter((link) => link.startsWith("http"))
        .reduce((acc, curr) => {
            acc[curr] = links[curr]
            return acc
        }, {})
}

/**
 * Makes sure the badge shows how many links we've saved
 */
async function updateBadge() {
    const links = await getLinks();
    var text = Object.keys(links).length || "";
    text = "" + text;
    await browser.browserAction.setBadgeText({text})
}

function timeout(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms)
    })
}

export {getLinks, updateBadge, timeout}
