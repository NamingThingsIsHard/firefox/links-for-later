const storage = browser.storage.local;

storage.get("options").then(({options = {}}) => {
    const $delayInput = document.querySelector("#delay-input");
    $delayInput.addEventListener("change", (event) => {
        options.delay = Number($delayInput.value);
        storage.set({options: options})
    })
    const $privateWindowInput = document.querySelector("#private-window-input");
    $privateWindowInput.addEventListener("change", async (event) => {
        options.privateWindow = $privateWindowInput.checked;
        let allowedIncognitoAccess = await browser.extension.isAllowedIncognitoAccess();
        if (options.privateWindow && !allowedIncognitoAccess) {
            browser.notifications.create({
                type: "basic",
                title: "Private mode inaccessible!",
                message: "Please allow \"Links for later\"" +
                    "to access private mode. Without it " +
                    "and this option activated, the extension" +
                    "will NOT work!",
                iconUrl: "data/icon-64.png"
            })
        }
        storage.set({options: options})
    })

    for (var optionName in options) {
        switch (optionName) {
            case "delay":
                $delayInput.value = options[optionName];
                break;
            case "privateWindow":
                $privateWindowInput.checked = options[optionName];
                break;
        }
    }
});
