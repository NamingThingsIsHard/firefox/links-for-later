import GetterTab from "./GetterTab.js"

export class PrivateGetterTab extends GetterTab {
    async _start(url){
        const win = await browser.windows.create({
            incognito: true,
            type: "panel",
            url: url,
            state: "minimized",
            titlePreface: "links-for-later"
        });
        const tab = win.tabs[0];
        this.tabId = tab.id;
        await this._connectTab()
    }
}
