import GetterTab from "./GetterTab.js"

export class RandomGetterTab extends GetterTab {

    constructor() {
        super();
        this.cookiestoreId = null;
    }

    async _start(url) {
        let createParams = {
            name: "tmp_links_for_later-" + Date.now(),
            color: "pink",
            icon: "fingerprint"
        };
        let context = await browser.contextualIdentities.create(createParams);
        this.cookiestoreId = context.cookieStoreId;
        let activeTabs = await browser.tabs.query({
            currentWindow: true, active: true
        });
        let tab = await browser.tabs.create({
            url: url,
            active: false,
            cookieStoreId: this.cookiestoreId,
            openerTabId: activeTabs[0].id
        });
        this.tabId = tab.id;

        await this._connectTab(tab);
    }

    _finalize() {
        super._finalize();

        if (this.cookiestoreId) {
            browser.contextualIdentities.remove(this.cookiestoreId);
        }
    }
}
