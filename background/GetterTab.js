import {timeout} from "../lib/common.js";

export default class GetterTab {

    constructor() {
        this.listeners = {
            removed: new Set(),
            onInformation: new Set(),
            error: new Set()
        }

        this.tabId = null
        this._loopShouldEnd = false

        this._onRemovedListener = this._onRemoved.bind(this);
    }

    /**
     * Creates a tab that will continuously ping information about the page
     *
     * Returns once the tab has been created and will ping
     *
     * @return Promise
     */
    async start(url) {
        try {
            browser.tabs.onRemoved.addListener(this._onRemovedListener);
            await this._start(url)
        } catch (e) {
            console.error(e);
            this._finalize();
            throw e
        }
    }

    /**

     * @abstract
     * @private
     */
    async _start(url) {
        throw "Not implemented"
    }

    async _connectTab() {
        // Work around for https://bugzilla.mozilla.org/show_bug.cgi?id=1397667
        // absolutely balls that the object tab isn't accessible yet >_>
        await timeout(1000);

        // low-key mute the tab
        browser.tabs.update(this.tabId, {muted: true})
        this._loopForInformation()
    }

    async _loopForInformation() {
        const STOP_ERROR = "Invalid tab";

        let execRes = null;
        let error = null;
        this._loopShouldEnd = false;
        while (!this._loopShouldEnd) {
            try {
                execRes = await browser.tabs.executeScript(this.tabId, {
                    file: "content-scripts/getInformation.js",
                    runAt: "document_start"
                })
                console.info("getInformation.js injected")
            } catch (e) {
                error = `${e}`
                if (!error.includes(STOP_ERROR)) {
                    await timeout(500)
                } else {
                    throw e
                }
            }
            if (execRes){
                this._trigger("onInformation", execRes[0])
            }
        }

    }

    /**
     */
    stop() {
        this._loopShouldEnd = true
        this._finalize()
    }

    _createTrigger(eventName) {
        let self = this;
        return function () {
            self.listeners[eventName].forEach((listener) => {
                listener.apply(null, arguments)
            })
        }
    }

    _trigger(eventName, ...args) {
        this._createTrigger(eventName).apply(this, args)
    }

    _onRemoved(removedTabId) {
        if (removedTabId !== this.tabId) {
            return
        }
        this._finalize();
        this._trigger("removed")
    }

    _finalize() {
        if (this.tabId) {
            browser.tabs.onRemoved.removeListener(this._onRemovedListener);
            browser.tabs.remove(this.tabId);
        }
    }
}
